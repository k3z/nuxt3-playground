const { me } = useAuth()

export default defineNuxtRouteMiddleware(async (_to, _from) => {
  const sessionUser = useSessionUser()

  me()

  if (!sessionUser.value) {
    return navigateTo({ name: 'login' })
  }
})
