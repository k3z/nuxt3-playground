import { getSessionUser } from '~~/server/utils/session'

export default defineEventHandler(async (event) => {
  const session = await getSessionUser(event)

  if (session) event.context.session = session
})
