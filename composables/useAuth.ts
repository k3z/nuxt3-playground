export const useAuth = () => {
  const runtimeConfig = useRuntimeConfig()
  const sessionUser = useSessionUser()

  const setUser = (user: any) => {
    sessionUser.value = user
  }

  const setCookie = (cookie: any) => {
    cookie.value = cookie
  }

  const login = async (
    email: string,
    password: string,
    rememberMe: boolean
  ) => {
    const data = await $fetch('/api/auth/login', {
      method: 'POST',
      body: {
        email,
        password,
        rememberMe,
      },
    })

    setUser(data.user)

    return sessionUser
  }

  const logout = async () => {
    const data = await $fetch('/api/auth/logout', {
      method: 'POST',
    })

    setUser(null)
  }

  const me = async () => {
    if (!sessionUser.value) {
      try {
        const data = await $fetch('/api/auth/me', {
          headers: useRequestHeaders(['cookie']) as HeadersInit,
        })
        setUser(data)
      } catch (error) {
        // setCookie(null)
      }
    }

    return sessionUser
  }

  return {
    login,
    logout,
    me,
  }
}
