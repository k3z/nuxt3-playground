// https://www.npmjs.com/package/nuxt-font-loader
export default {
  local: [
    {
      src: '/fonts/graphik/Graphik-Regular-Web.woff2',
      family: 'Graphik Web',
      weight: '400',
      class: 'graphik-web-regular',
    },
    {
      src: '/fonts/graphik/Graphik-Bold-Web.woff2',
      family: 'Graphik Web',
      weight: '700',
      class: 'graphik-web-bold',
    },
    {
      src: 'fonts/IBM_Plex_Mono/IBM_Plex_Mono-400-latin5.woff2',
      family: 'IBM Plex Mono',
      weight: '400',
      class: 'ibm-plex-mono',
    },
  ],
}
