// https://nuxt.com/docs/getting-started/seo-meta
export default {
  title: 'Nuxt3 playground',
  meta: [
    {
      name: 'description',
      content:
        'Ce projet Nuxt3 intègre une authentification basée sur des cookies',
    },
  ],
}
