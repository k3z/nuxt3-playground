import { createI18n } from 'vue-i18n'

// https://vue-i18n.intlify.dev/guide/integrations/nuxt3.html
export default defineNuxtPlugin(({ vueApp }) => {
  const i18n = createI18n({
    legacy: false,
    globalInjection: true,
    locale: 'fr',
    messages: {
      fr: {
        ENABLED: 'activé',
        DISBLED: 'désactivé',
      },
    },
  })

  vueApp.use(i18n)
})
