import md from 'markdown-it'

export default defineNuxtPlugin(() => {
  const renderer = md({
    preset: 'default',
    linkify: true,
    breaks: true,
    use: ['markdown-it-div', 'markdown-it-attrs'],
  })
  return {
    provide: {
      mdRenderer: renderer,
    },
  }
})
