# Dockerfile
FROM node:17-alpine

ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=3000

RUN wget -qO /bin/pnpm "https://github.com/pnpm/pnpm/releases/latest/download/pnpm-linuxstatic-x64" && chmod +x /bin/pnpm


# create destination directory
RUN mkdir -p /srv/frontend
WORKDIR /srv/frontend

# copy the app, note .dockerignore
COPY ./package.json  /srv/frontend
COPY ./pnpm-lock.yaml  /srv/frontend
RUN pnpm install --shamefully-hoist
COPY . /srv/frontend
RUN pnpm run build

EXPOSE 3000

# ADD start.sh /start.sh
# CMD /start.sh


CMD [ "node", ".output/server/index.mjs" ]
