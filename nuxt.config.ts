// https://nuxt.com/docs/api/configuration/nuxt-config
// https://leighdinaya.com/blog/configure-eslint-prettier-husky-commitlint-nuxt3/

const ONE_DAY = 60 * 60 * 24 * 1000
const ONE_WEEK = ONE_DAY * 7

export default defineNuxtConfig({
  runtimeConfig: {
    https: false,
    cookieName: process.env.COOKIE_NAME || '__session',
    cookieSecret: process.env.COOKIE_SECRET || 'secret',
    cookieExpires: parseInt(
      process.env.COOKIE_REMEMBER_ME_EXPIRES || ONE_DAY.toString(),
      10
    ), // 1 day
    cookieRememberMeExpires: parseInt(
      process.env.COOKIE_REMEMBER_ME_EXPIRES || ONE_WEEK.toString(),
      10
    ),
    public: {
      debug: true,
      env: process.env.NODE_ENV,
      apiBase: '/api',
    },
  },
  app: {
    head: require('../config/head.ts'),
  },
  css: ['bootstrap/dist/css/bootstrap.css'],
  modules: [
    'bootstrap-vue-3/nuxt',
    '@pinia/nuxt',
    'nuxt-font-loader',
    // https://github.com/nuxt-modules/icon
    'nuxt-icon',
  ],
  fontLoader: require('../config/fontLoader.ts'),
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/style/_variables.scss";',
        },
      },
    },
  },
})
